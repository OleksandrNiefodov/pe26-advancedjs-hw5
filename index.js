'use srtict'
/*
При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts
Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад доданий в репозиторії групи, посилання на ДЗ вище), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
*/

   

async function getUsers() {
    try {
      const response = await fetch('https://ajax.test-danit.com/api/json/users');
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching users:', error);
    }
  }
  
  
  async function getPosts() {
    try {
      const response = await fetch('https://ajax.test-danit.com/api/json/posts');
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching posts:', error);
    }
  }

 
function displayPosts(users, posts) {
    const postsContainer = document.getElementById('posts-container');
  
    posts.forEach(post => {
      const user = users.find(user => user.id === post.userId);
      if (user) {
        const card = new Card(post.title, post.body, user.name, user.email, post.id);
        postsContainer.appendChild(card.render());
      }
    });
  }


  class Card {
    constructor(title, body, userName, userEmail, postId) {
      this.title = title;
      this.body = body;
      this.userName = userName;
      this.userEmail = userEmail;
      this.postId = postId;
    }
  
    render() {
      const cardElement = document.createElement('div');
      cardElement.classList.add('card');
  
      const titleElement = document.createElement('h2');
      titleElement.textContent = this.title;
  
      const bodyElement = document.createElement('p');
      bodyElement.textContent = this.body;
  
      const userInfoElement = document.createElement('p');
      userInfoElement.textContent = `Posted by ${this.userName} (${this.userEmail})`;
  
      const deleteButton = document.createElement('button');
      deleteButton.textContent = 'Delete';
      deleteButton.addEventListener('click', this.deletePost.bind(this));
  
      cardElement.appendChild(titleElement);
      cardElement.appendChild(bodyElement);
      cardElement.appendChild(userInfoElement);
      cardElement.appendChild(deleteButton);
  
      return cardElement;
    }
  
    async deletePost() {
      try {
        const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
          method: 'DELETE'
        });
        if (response.ok) {
          this.removeCard();
        } else {
          throw new Error('Failed to delete post');
        }
      } catch (error) {
        console.error('Error deleting post:', error);
      }
    }
  
    removeCard() {
      const cardElement = document.getElementById(`post-${this.postId}`);
      if (cardElement) {
        cardElement.remove();
      }
    }
  }


  document.addEventListener('DOMContentLoaded', async () => {
    const users = await getUsers();
    const posts = await getPosts();
  
    displayPosts(users, posts);
  });
